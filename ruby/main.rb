require 'json'
require 'socket'

# Don't let anything buffer stdout, always print immediately
$stdout.sync = true

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

# Straight piece of track.
class Straight
  attr_reader :length, :switch

  def initialize(length, switch)
    @length = length
    @switch = switch
  end
end

# Turn in the track.
class Bend
  attr_reader :radius, :angle

  def initialize(radius, angle, switch)
    @radius = radius
    @angle = angle
    @switch = switch
  end
end

# Information about the track.
class Track
  attr_reader :id, :name, :pieces, :lanes, :startingPoint_x, :startingPoint_y, :startingPoint_angle

  def initialize(id, name, pieces, lanes, startingPoint)
    @id = id
    @name = name
    @pieces = []
    @lanes = []
    @startingPoint_x = startingPoint["position"]["x"]
    @startingPoint_y = startingPoint["position"]["y"]
    @startingPoint_angle = startingPoint["angle"]

    # Add all of the pieces of track to a @pieces array
    pieces.each do |piece|
      # If it's a straight
      if(piece.has_key?("length"))
        switch = piece.has_key?("switch") ? piece["switch"] : false

        @pieces.push(Straight.new(piece["length"], switch))
      # if it's a bend
      else
        switch = piece.has_key?("switch") ? piece["switch"] : false

        @pieces.push(Bend.new(piece["radius"], piece["angle"], switch))
      end
    end

    # Add each lane to a lanes array. The lane value is the distance from the
    # center of the road, negative being to the left, positive to the right.
    lanes.each do |lane|
      @lanes.push(lane["distanceFromCenter"])
    end
  end

  # This is tricky. Depending on the direction of the turn, the lane you're
  # in can either add to or subtract from the total distance you travel
  # in the turn.
  def geExpectedAngleAtPositionInPiece(pieceIndex, inPieceDistance, lane)
    starting_angle = @startingPoint_angle

    # Figure out the angle going into the pieceIndex
    (0...pieceIndex).each do |idx|
      # For straights, our angle doesn't change, only for bends
      if(@pieces[idx].is_a?(Bend))
        starting_angle += @pieces[idx].angle
      end
    end

    # Now if we're in a bend, we need to figure out how far into it we are
    if(@pieces[pieceIndex].is_a?(Bend))
      lane_direction_from_center = @lanes[lane] < 0 ? :left : :right
      lane_distance_from_center = @lanes[lane].abs

      bend_direction = @pieces[pieceIndex].angle < 0 ? :left : :right
      bend_radius = @pieces[pieceIndex].radius.abs
      bend_angle = @pieces[pieceIndex].angle.abs

      # If we're to the left of center and turning left, or to the right
      # of center and turning right, we're on the inside lane, otherwise
      # we're in the outside lane
      if((lane_direction_from_center == :left and bend_direction == :left) or
         (lane_direction_from_center == :right and bend_direction == :right))
        bend_radius -= lane_distance_from_center
      else
        bend_radius += lane_distance_from_center
      end

      circumference = 2.0 * Math::PI * (bend_radius)
      total_piece_distance = circumference * (bend_angle / 360.0)
      change_in_car_angle = bend_angle * (inPieceDistance / total_piece_distance)

      # If we're turning left, subtract the angle, otherwise add it
      if(bend_direction == :left)
        starting_angle -= change_in_car_angle
      else
        starting_angle += change_in_car_angle
      end
    end

    puts starting_angle % 360
    starting_angle % 360
  end
end

# A car.
class Car
  attr_reader :name, :color, :length, :width, :guideFlagPosition
  attr_accessor :angle, :pieceIndex, :inPieceDistance, :startLaneIndex, :endLaneIndex, :lap

  # Don't really need to do anything here, set properties via accessors
  def initialize(name, color, length, width, guideFlagPosition)
    @name = name
    @color = color
    @length = length
    @width = width
    @guideFlagPosition = guideFlagPosition
  end
end

# Handle the messages related to game play.
class GamePlay
  def initialize()
  end

  def handleGameInit(json)
    track = json["data"]["race"]["track"]
    car_id = track["id"]
    car_name = track["name"]
    car_pieces = track["pieces"]
    car_lanes = track["lanes"]
    car_startingPoint = track["startingPoint"]

    @track = Track.new(car_id, car_name, car_pieces, car_lanes, car_startingPoint)
    #puts @track.inspect
    #puts ""

    @cars = []
    cars = json["data"]["race"]["cars"]
    cars.each do |car|
      car_id = car["id"]["name"]
      car_color = car["id"]["color"]
      car_length = car["dimensions"]["length"]
      car_width = car["dimensions"]["width"]
      car_guideFlagPosition = car["dimensions"]["guideFlagPosition"]

      @cars.push(Car.new(car_id, car_color, car_length, car_width, car_guideFlagPosition))
    end
    #puts @cars.inspect
    #puts ""
  end

  def handleCarPositions(json)
    json["data"].each do |car_json|
      car_json_name = car_json["id"]["name"]
      car_json_color = car_json["id"]["color"]
      car_json_angle = car_json["angle"]
      car_json_pieceIndex = car_json["piecePosition"]["pieceIndex"]
      car_json_inPieceDistance = car_json["piecePosition"]["inPieceDistance"]
      car_json_startLaneIndex = car_json["piecePosition"]["lane"]["startLaneIndex"]
      car_json_endLaneIndex = car_json["piecePosition"]["lane"]["endLaneIndex"]
      car_json_lap = car_json["piecePosition"]["lap"]

      @cars.each do |car|
        if(car.name == car_json_name and car.color == car_json_color)
          #We found a matching car
          car.angle = car_json_angle
          car.pieceIndex = car_json_pieceIndex
          car.inPieceDistance = car_json_inPieceDistance
          car.startLaneIndex = car_json_startLaneIndex
          car.endLaneIndex = car_json_endLaneIndex
          car.lap = car_json_lap
        end
      end

      outputSlip(@cars, @track)
    end

    #puts @cars.inspect
    #puts ""
  end

  # Stubbed for now
  def handleYourCar(json)
  end

  def outputSlip(cars, track)
    cars.each do |car|
      track.geExpectedAngleAtPositionInPiece(car.pieceIndex, car.inPieceDistance, car.endLaneIndex)
    end
  end
end






# Parse out a local JSON file that represents a game played, to make sure
# that data structures are populated correctly, etc.
class JsonParserTest
  def initialize(file)
    json = File.read(file)
    obj = JSON.parse(json)

    @GamePlay = GamePlay.new()

    # For each of the fake JSON message we see, handle them
    obj.each_key do |key|
      arbitrateMessages(obj[key])
    end

  end

  # Look at the "msgType json value to see what kind of a message we're
  # dealing with.
  def arbitrateMessages(json)
    case(json["msgType"])
    when "gameInit"
      @GamePlay.handleGameInit(json)
    when %r{carPositions}
      @GamePlay.handleCarPositions(json)
    end
  end
end

class OurBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    @GamePlay = GamePlay.new()
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)

    puts "{"

    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          @GamePlay.handleCarPositions(message)

          tcp.puts throttle_message(0.4)
        else
          case msgType
            when 'yourCar'
              @GamePlay.handleYourCar(message)
            when 'gameInit'
              @GamePlay.handleGameInit(message)
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          end

          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

class GenerateJSONInput
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    @count = 0
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)

    # Starting JSON {
    puts "{"

    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          # Pretty print the JSON for the carPosition. There cannot be
          # duplicate entries in the JSON, so append a count
          puts "  \"carPositions#{@count}\" :"
          @count = @count + 1
          puts JSON.pretty_generate(message)
          puts ","

          # Just to keep the game moving
          tcp.puts throttle_message(0.4)
        else
          case msgType
            # Pretty print the JSON for the yourCar.
            when 'yourCar'
              puts "  \"yourCar\" :"
              puts JSON.pretty_generate(message)
              puts ","
            # Pretty print the JSON for the gameInit.
            when 'gameInit'
              puts "  \"gameInit\" :"
              puts JSON.pretty_generate(message)
              puts ","
          end
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

#GenerateJSONInput.new(server_host, server_port, bot_name, bot_key)
#OurBot.new(server_host, server_port, bot_name, bot_key)
JsonParserTest.new("input.json")
